    // ROGGED OTG - inicialización variables
    let stopRoulette = false;

    // The responisve parameter is the only difference needed to make a wheel responsive.
    
    let theWheel = new Winwheel({
        'numSegments'  : 6,
        'textFontSize' : 22,
        'textAlignment'     : 'outer',
        'textOrientation'   : 'curved',
        'textMargin'        : 45,
        'responsive'   : true,  // This wheel is responsive!
        'drawMode'          : 'segmentImage',
        
        'segments'     :
        [
            {'image' : 'assets/img/1.png', 'text' : '+100'}, // 0 - 60 (10-50)
            {'image' : 'assets/img/2.png', 'text' : '+200'}, // 61 - 120 (70 - 110)
            {'image' : 'assets/img/3.png', 'text' : '+300'}, // 121 - 180 (130 - 170)
            {'image' : 'assets/img/4.png', 'text' : '+400'}, // 181 - 240 (190 - 230)
            {'image' : 'assets/img/5.png', 'text' : '+500'}, // 241 - 300 (250 - 290)
            {'image' : 'assets/img/6.png', 'text' : '+600'} // 301 - 360 (310 - 350)
        ],
        //'outerRadius': 70,
        //'innerRadius': 150,
        'pins' :
        {
            'outerRadius': 6,
            'responsive' : true, // This must be set to true if pin size is to be responsive.
        },
        'animation' :
        {
            'type'     : 'spinToStop',
            'duration' : 5, // duración influye en velocidad a mayor será más lento
            'spins'    : 3, // número de giros influye en duración
            'callbackFinished' : spinsend,
            'stopAngle':10
        },
        
    });

    // ROGGED OTG - click in zone for spin
    let canvas = document.getElementById('canvas');
    canvas.onclick = function (evt)
    {
        var i, xDiff, yDiff, dist, result, cX, cY, startLength; 
        xPos=null; yPos=null; circX=null; circY=null; 
        evt= evt || event;
        xPos=evt.offsetX || evt.pageX;
        yPos=evt.offsetY || evt.pageY;
        cX=canvas.width/2
        cY=canvas.height/2
        xDiff=Math.abs(cX-xPos);
        yDiff=Math.abs(cY-yPos);
        dist=Math.sqrt(Math.pow(xDiff,2)+Math.pow(yDiff,2));
        if(dist <=75 && !stopRoulette){
            // sping ruleta
            theWheel.stopAnimation(false);
            theWheel.rotationAngle = theWheel.rotationAngle % 360;
            theWheel.startAnimation();            
        }
    }    

    function spinsend(indicatedSegment)
    {
        stopRoulette=true;
        alert("Ha ganado " + indicatedSegment.text);
    }